# Create your views here.
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import generics
from rest_framework.filters import OrderingFilter

from channel_stats.models import Channel, Video
from channel_stats.serializers import ChannelSerializer, VideoModelSerializer
from channel_stats.utils.filters import TagFilter
from channel_stats.utils.tasks import scrape_channel_data
from channel_stats.utils.youtube_api import YoutubeApiV3


class ChannelListCreate(generics.ListCreateAPIView):
    queryset = Channel.objects.all()
    serializer_class = ChannelSerializer

    def perform_create(self, serializer):
        channel_id = serializer.validated_data.get("id")
        data_received, channel_title = YoutubeApiV3(channel_id).get_channel_title
        if not data_received:
            channel_title = f"Channel - {channel_id}"
        serializer.save(title=channel_title)
        scrape_channel_data(channel_id=channel_id, repeat=1200)


class VideoListView(generics.ListAPIView):
    filter_backends = (OrderingFilter, DjangoFilterBackend)
    filter_class = TagFilter
    ordering_fields = ("id", "created", "performance")
    serializer_class = VideoModelSerializer

    def get_queryset(self):
        return Video.objects.filter(channel_id=self.kwargs.get("id"))
