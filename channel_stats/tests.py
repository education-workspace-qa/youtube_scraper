from django.test import TestCase

# Create your tests here.


class ChannelsTestCase(TestCase):
    fixtures = ["fixture.json"]

    def test_get_all_channels(self):
        response = self.client.get(f"/api/channels/")
        data = response.json()
        self.assertEqual(len(data["results"]), 2)
        self.assertEqual(response.status_code, 200)

    def test_add_new_channel(self):
        payload = dict(id="UCmhlNRBKhkijmy53Cre3slg")
        response = self.client.post(
            f"/api/channels/", data=payload, content_type="application/json"
        )
        data = response.json()
        self.assertEqual(data["id"], "UCmhlNRBKhkijmy53Cre3slg")
        self.assertEqual(response.status_code, 201)

    def test_add_existing_channel(self):
        payload = dict(id="UCtYXvB25jRJC9pKJQooYktQ")
        response = self.client.post(
            f"/api/channels/", data=payload, content_type="application/json"
        )
        self.assertEqual(
            response.json()["id"][0], "channel with this channel id already exists."
        )
        self.assertEqual(response.status_code, 400)


class VideoTestCase(TestCase):
    fixtures = ["fixture.json"]

    def test_get_all_channel_videos(self):
        response = self.client.get(f"/api/channels/UC3pV6eELigzhTjzBUPmT6_A/videos/")
        data = response.json()
        self.assertEqual(data["count"], 307)
        self.assertEqual(len(data['results']), 25)
        self.assertEqual(response.status_code, 200)

    def test_get_all_channel_videos_by_tag(self):
        tag = "Derral Eves"
        response = self.client.get(f"/api/channels/UC3pV6eELigzhTjzBUPmT6_A/videos/?tag={tag}")
        data = response.json()
        self.assertEqual(data["count"], 291)
        self.assertEqual(len(data['results']), 25)
        self.assertEqual(response.status_code, 200)

    def test_get_all_channel_videos_by_non_existing_tag(self):
        tag = "random tag"
        response = self.client.get(f"/api/channels/UC3pV6eELigzhTjzBUPmT6_A/videos/?tag={tag}")
        data = response.json()
        self.assertEqual(data["count"], 0)
        self.assertEqual(len(data['results']), 0)
        self.assertEqual(response.status_code, 200)
