from django.urls import path

from channel_stats import views

urlpatterns = [
    path("channels/", views.ChannelListCreate.as_view(), name="channels"),
    path("channels/<str:id>/videos/", views.VideoListView.as_view(), name="videos"),
]
