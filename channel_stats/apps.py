from django.apps import AppConfig


class ChannelStatsConfig(AppConfig):
    name = 'channel_stats'
