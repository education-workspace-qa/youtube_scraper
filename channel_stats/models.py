from django.db import models
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _
from django.contrib.postgres.fields import JSONField


class Channel(models.Model):
    id = models.CharField(_("channel id"), primary_key=True, max_length=100)
    title = models.CharField(_("channel title"), null=True, blank=True, max_length=100)
    statistics = JSONField(_("statistics"), default=dict, null=False, blank=True)


class Tag(models.Model):
    title = models.CharField(_("tag title"), max_length=100)


class Video(models.Model):
    id = models.CharField(_("video id"), primary_key=True, max_length=100)
    channel = models.ForeignKey(
        Channel, on_delete=models.CASCADE, related_name="videos"
    )
    performance = models.FloatField(_("performance"), null=True, blank=True)
    tags = models.ManyToManyField(Tag)
    created = models.DateTimeField(_("created datetime"), null=True, blank=True)
    statistics = JSONField(_("statistics"), default=dict, null=True, blank=True)
    last_updated = models.DateTimeField(
        _("last updated datetime"), null=False, blank=True, default=timezone.now
    )
