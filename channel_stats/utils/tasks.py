from background_task import background

from channel_stats.utils.youtube_api import YoutubeApiV3


@background(schedule=10)
def scrape_channel_data(channel_id):
    print(f"Receiving data from channel {channel_id}")
    client = YoutubeApiV3(channel_id)
    client.update_or_create_channel_videos_info()
