import django_filters

from channel_stats.models import Video


class TagFilter(django_filters.FilterSet):
    tag = django_filters.Filter(field_name="tags__title", lookup_expr="contains")

    class Meta:
        model = Video
        fields = ("tag",)
