from typing import List, Union

from channel_stats.models import Tag


def get_tags_objects_list(tags: List[str]) -> Union[None, List[Tag]]:
    if not tags:
        return
    tags_list = list()
    for tag_title in tags:
        tags_list.append(get_or_create_tag(tag_title))
    return tags_list


def get_or_create_tag(title: str) -> Tag:
    tag, created = Tag.objects.get_or_create(title=title)
    return tag
