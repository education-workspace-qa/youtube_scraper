import json
from statistics import median, StatisticsError
from typing import Tuple

from django.utils import timezone
from googleapiclient.discovery import build
from googleapiclient.errors import HttpError

from channel_stats.models import Video, Channel
from channel_stats.utils.db_utils import get_tags_objects_list
from youtube_channel_scraper.settings import API_KEY


class YoutubeApiV3:
    """
    Module created for scraping channel videos information from Youtube API V3
    """
    def __init__(self, channel_id: None):
        self.channel_id = channel_id
        self.client = build(serviceName="youtube", version="v3", developerKey=API_KEY)

    def _get_youtube_search_video_data(self, page_token: str = None):
        """
        Args:
            page_token: next page token

        Returns:
            API data status (True/False), data( error)
        """
        try:
            request_data = (
                self.client.search()
                .list(
                    part="id",
                    channelId=self.channel_id,
                    maxResults=50,
                    type="video",
                    pageToken=page_token,
                )
                .execute()
            )
            return True, request_data
        except HttpError as error:
            return False, json.loads(error.content.decode())

    def _get_youtube_video_info(self, video_id: str):
        """
        Args:
            video_id: Target video id to get data

        Returns:
            API data status (True/False), data( error)
        """
        try:
            request_data = (
                self.client.videos()
                .list(part="snippet, statistics", id=video_id)
                .execute()
            )
            return True, request_data
        except HttpError as error:
            return False, json.loads(error.content.decode())

    @property
    def get_channel_title(self) -> Tuple[bool, str]:
        """
        Function to get channel title
        Returns:
            Boolean (content received True/False), content (or error body)

        """
        try:

            request_data = (
                self.client.channels()
                .list(part="snippet", id=self.channel_id)
                .execute()
            )
        except HttpError as error:
            return False, json.loads(error.content.decode())
        if not request_data["pageInfo"]["totalResults"]:
            return False, "Channel Not found"
        return True, request_data["items"][0]["snippet"]["title"]

    @property
    def _get_channel_video_id_list(self):
        """
        Property to retrieve channel video ids
        Returns:
            List of video ids
        """
        all_videos_ids = list()
        data_received, content = self._get_youtube_search_video_data()
        # Here I would add a logger (with notification) if no data_received
        if not data_received or not content["pageInfo"]["totalResults"]:
            print(content)
            return False, content
        next_page_token = content.get("nextPageToken")
        all_videos_ids += [
            video_data["id"]["videoId"] for video_data in content["items"]
        ]
        while next_page_token:
            data_received, content = self._get_youtube_search_video_data()
            # Here I would logg error if no data_received
            all_videos_ids += [
                video_data["id"]["videoId"] for video_data in content["items"]
            ]
            next_page_token = content.get("nextPageToken")

        return True, all_videos_ids

    def update_or_create_channel_videos_info(self):
        """
        Function to fetch videos info (statistics) from Youtube API
        Includes calculation of channel and video performance

        """
        data_received, video_ids = self._get_channel_video_id_list
        if not data_received:
            print(video_ids)
            # Here I would add a logger (with notification) if no data_received
            return video_ids
        for id in video_ids:
            data_received, content = self._get_youtube_video_info(id)
            # Here I would logg error if no data_received
            if not data_received or not content["pageInfo"]["totalResults"]:
                return
            tags = content["items"][0]["snippet"].get("tags")
            data = dict(
                id=content["items"][0]["id"],
                channel_id=self.channel_id,
                statistics=content["items"][0]["statistics"],
                created=content["items"][0]["snippet"]["publishedAt"],
                last_updated=timezone.now(),
            )
            video, created = Video.objects.update_or_create(
                id=data["id"], defaults={**data}
            )
            if tags:
                video.tags.add(*get_tags_objects_list(tags))
        self._calculate_channel_video_performance()

    def _calculate_channel_video_performance(self):
        """
        Function for video and channel performance calculation

        """
        try:
            channel = Channel.objects.get(id=self.channel_id)
        except Channel.DoesNotExist:
            channel = Channel.objects.create(id=self.channel_id)
        channel_videos = Video.objects.filter(channel_id=self.channel_id).only(
            "performance", "statistics"
        )
        if not channel_videos.exists():
            print("No videos to calculate")
            return
        all_videos_median = median(
            [int(video.statistics["viewCount"]) for video in channel_videos]
        )

        channel.statistics = {
            **channel.statistics,
            "median_views_count": all_videos_median,
        }
        channel.save()
        for video in channel_videos:
            video.performance = round(
                int(video.statistics["viewCount"]) / all_videos_median, ndigits=2
            )
            video.last_updated = timezone.now()
        Video.objects.bulk_update(
            channel_videos, fields=["performance", "last_updated"]
        )
