from django.contrib import admin


# Register your models here.
from channel_stats.models import Channel, Tag, Video

admin.register(Channel)
admin.register(Tag)
admin.register(Video)