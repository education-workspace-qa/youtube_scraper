from rest_framework import serializers

from channel_stats.models import Channel, Video


class ChannelSerializer(serializers.ModelSerializer):
    class Meta:
        model = Channel
        fields = "__all__"


class VideoModelSerializer(serializers.ModelSerializer):
    class Meta:
        model = Video
        fields = "__all__"
