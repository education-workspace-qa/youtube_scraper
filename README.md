## Youtube channel videos info scraper

## Installation
 
1. Clone the repository `$ git clone https://tonkoyarik@bitbucket.org/tonkoyarik/youtube_scraper.git`.

2. Create a virtual environment running Python 3.0-3.6 `$ cd youtube_channel_scraper && virtualenv -p python3.7 VRS_ENV`
3. Install the required packages for the app `$ pip install requirements.txt`
4. Create database locally according to credentials mentioned in settings.py DATABASES
4. Run the Django migrations `$ python manage.py migrate`.
5. Test to see if the app runs `$ python manage.py runserver`.
6. Run django background tasks in serapate terminal `$ python manage.py process_tasks`

### To run tests:
`python manage.py test`

Now you can test the application:
Once you open `localhost:8000/` you will see a documentation page where you can test endpoints
E.g Once you add a channel with channel ID e.g  `UCtYXvB25jRJC9pKJQooYktQ` or `UC3pV6eELigzhTjzBUPmT6_A`, you will get a response 201 and background 
task will start fetching channel video information. Soon you will see new video information appear in the database as well as new tags.
Then you can try to get list of videos and filter them with `tag` querystring parameter.

#### Note:
 In current application I used django background tasks but in real app I'd rather use Celery and RabbitMQ broker to manage scheduled tasks.
 Once a quota limit is exceeded, a background task will still be running but printing information about exceeded quota, this part requires some optimization as stopping a task from executing until next day would be a better idea.